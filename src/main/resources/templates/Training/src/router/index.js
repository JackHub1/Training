import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/index';
import ModifyUser from '@/vues/ModifyUser';

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/',
    name: 'Index',
    component: Index,
  },
  {
    path: '/createUser',
    name: 'createUser',
    component: ModifyUser,
  },
  {
    path: '/updateUser',
    name: 'updateUser',
    component: ModifyUser,
  },
  ],
});
