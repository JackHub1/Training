package com.augmentu.mediacloud.training.util;

import java.util.Calendar;
import java.util.Date;

public class GetMaxAndMinDate {

    /**
     * Get the must small date
     * 
     */
    public static Date getMinDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.getMinimum(Calendar.YEAR),
                calendar.getMinimum(Calendar.MONTH),
                calendar.getMinimum(Calendar.DATE),
                calendar.getMinimum(Calendar.HOUR_OF_DAY),
                calendar.getMinimum(Calendar.MINUTE),
                calendar.getMinimum(Calendar.SECOND)
                );
        Date minDate = calendar.getTime();
        return minDate;
    }

    /**
     * Get the must big date
     * 
     */
    public static Date getMaxDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.getMaximum(Calendar.YEAR),
                calendar.getMaximum(Calendar.MONTH),
                calendar.getMaximum(Calendar.DATE),
                calendar.getMaximum(Calendar.HOUR_OF_DAY),
                calendar.getMaximum(Calendar.MINUTE), 
                calendar.getMaximum(Calendar.SECOND));
        Date maxDate = calendar.getTime(); 
        return maxDate;
    }

}
