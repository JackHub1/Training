package com.augmentu.mediacloud.training.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "userinfo")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private ObjectId _id; // mongo id
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; // postgresql id
    private String name;
    private String sex;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;
    private String originPlace;
    private String nation;
    private String post;
    private Date createTime = new Date();
    private Date deleteTime;
    private boolean isDelete;

    public User() {
        super();
    }

    public User(String name, String sex, Date birthday, String originPlace, String nation, String post, Date createTime,
            Date deleteTime, boolean isDelete) {
        super();
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.originPlace = originPlace;
        this.nation = nation;
        this.post = post;
        this.createTime = createTime;
        this.deleteTime = deleteTime;
        this.isDelete = isDelete;
    }

    public String get_id() {

        if (this._id != null) {
            return this._id.toString();
        }

        return null;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return this.birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getOriginPlace() {
        return originPlace;
    }

    public void setOriginPlace(String originPlace) {
        this.originPlace = originPlace;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

}
