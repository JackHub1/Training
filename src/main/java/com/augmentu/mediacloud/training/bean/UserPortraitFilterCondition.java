package com.augmentu.mediacloud.training.bean;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class UserPortraitFilterCondition {
    private String name;
    private String sex;
    private String post;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDay;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDay;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Date getStartDay() {
        return startDay;
    }

    public void setStartDay(Date startDay) {
        this.startDay = startDay;
    }

    public Date getEndDay() {
        return endDay;
    }

    public void setEndDay(Date endDay) {
        this.endDay = endDay;
    }

}
