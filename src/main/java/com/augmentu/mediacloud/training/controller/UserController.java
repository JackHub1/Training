package com.augmentu.mediacloud.training.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.augmentu.mediacloud.training.bean.User;
import com.augmentu.mediacloud.training.bean.UserPortraitFilterCondition;
import com.augmentu.mediacloud.training.service.UserService;

@CrossOrigin
@RestController
@RequestMapping(value = "/users")
public class UserController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserService userService;

    /**
     * Save user
     * 
     * @param user
     * @return save user
     */
    @PostMapping("/")
    public User save(@RequestBody User user) {
        User resultUser = userService.saveUser(user);
        return resultUser;
    }

    /**
     * Search user
     * 
     * @param CurrentPageNum
     * @return Page user
     */
    @GetMapping("/find")
    public Page<User> find(UserPortraitFilterCondition user, @RequestParam(name = "pageNum") String pageNum) {
        int num = 1;

        try {
            num = Integer.parseInt(pageNum);
        } catch (Exception e) {
            logger.warn("参数转换失败" + e.getMessage());
        }

        return userService.findUser(user, num);
    }

    /**
     * Update user information
     * 
     * @param user
     * @param _id
     * @return updated user
     */
    @PutMapping("/")
    public void updateUser(@RequestBody User user) {
        userService.saveUser(user);
    }

    /**
     * Multiple delete user
     * 
     * @param ids
     */
    @DeleteMapping("/delete")
    public void deleteMultipleUser(@RequestParam(name = "deleteIdGroup") String[] ids) {
        userService.deleteMultipleUser(ids);
    }

    /**
     * Find user by id
     * 
     * @param id
     * @return Find user
     */
    @GetMapping("/{id}")
    public User findUserById(@PathVariable String id) {
        return userService.findUserById(id);
    }

}
