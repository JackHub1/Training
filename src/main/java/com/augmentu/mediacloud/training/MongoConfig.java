package com.augmentu.mediacloud.training;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.stereotype.Component;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

@Component
@Profile("mongodb")
public class MongoConfig extends AbstractMongoConfiguration {

    @Value("${spring.data.mongo.host}")
    private String mongoHost;

    @Value("${spring.data.mongo.port}")
    private int mongoPort;

    @Value("${spring.data.mongo.database}")
    private String mongoDb;

    @Override
    public String getDatabaseName() {
        return mongoDb;
    }

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient(new ServerAddress(mongoHost, mongoPort));
    }
}