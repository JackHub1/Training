package com.augmentu.mediacloud.training.constants;

public class UserConstants {

    // User constant
    public static final String NAME = "name";
    public static final String SEX = "sex";
    public static final String BIRTHDAY = "birthday";
    public static final String IS_DELETE = "isDelete";
    public static final String POST = "post";

    // UserPortraitFilterCondition
    public static final String CREATE_TIME = "createTime";
    public static final String MALE = "男";
    public static final String FAMALE = "女";
}
