package com.augmentu.mediacloud.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserApplication  {

    /**
     * Project entry
     * 
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}