package com.augmentu.mediacloud.training.service;

import org.springframework.data.domain.Page;

import com.augmentu.mediacloud.training.bean.User;
import com.augmentu.mediacloud.training.bean.UserPortraitFilterCondition;

public interface UserService {

    /**
     * Save user
     * 
     * @param user
     * @return saved user
     */
    User saveUser(User user);

    /**
     * Find user and Paging the user
     * 
     * @param user
     * @return
     */
    Page<User> findUser(UserPortraitFilterCondition user, Integer pageNum);

    /**
     * Find user by id
     * 
     * @param id
     * @return Find user
     */
    User findUserById(String id);

    /**
     * Delete user
     * 
     * @param ids
     */
    void deleteMultipleUser(String[] ids);

}