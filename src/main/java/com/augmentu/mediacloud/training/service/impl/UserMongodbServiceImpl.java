package com.augmentu.mediacloud.training.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.augmentu.mediacloud.training.bean.User;
import com.augmentu.mediacloud.training.bean.UserPortraitFilterCondition;
import com.augmentu.mediacloud.training.constants.UserConstants;
import com.augmentu.mediacloud.training.dao.mongodbDao.UserMongodbRepository;
import com.augmentu.mediacloud.training.service.UserService;
import com.augmentu.mediacloud.training.util.GetMaxAndMinDate;

@Service
@Profile("mongodb")
public class UserMongodbServiceImpl implements UserService {
    @Autowired
    private UserMongodbRepository userMongodbRepository;

    @Override
    public User saveUser(User user) {
        return userMongodbRepository.save(user);
    }

    /**
     * Set filter condition
     * 
     * @param user
     */
    private void setFindUserCondition(UserPortraitFilterCondition user) {

        if (user.getName() == null || user.getName().equals("")) {
            user.setName(".");
        }

        if (user.getSex() == null || user.getSex().equals("")) {
            user.setSex(".");
        }

        if (user.getStartDay() == null || user.getStartDay().equals("")) {
            user.setStartDay(GetMaxAndMinDate.getMinDate());
        }

        if (user.getEndDay() == null || user.getEndDay().equals("")) {
            user.setEndDay(GetMaxAndMinDate.getMaxDate());
        }

        if (user.getPost() == null || user.getPost().equals("")) {
            user.setPost(".");
        }
    }

    @Override
    public Page<User> findUser(UserPortraitFilterCondition user, Integer pageNum) {
        Page<User> resultUsers = null;
        setFindUserCondition(user);
        resultUsers = userMongodbRepository.findUser(user.getName(), user.getSex(), user.getStartDay(),
            user.getEndDay(), user.getPost(),
            new PageRequest(pageNum - 1, 10, new Sort(Direction.DESC, UserConstants.CREATE_TIME)));
        return resultUsers;
    }

    @Override
    public User findUserById(String id) {
        ObjectId newId = new ObjectId(id);
        User resultUser = userMongodbRepository.findOne(newId);
        return resultUser;
    }

    @Override
    public void deleteMultipleUser(String[] ids) {
        List<User> users = new ArrayList<>();

        for (String id : ids) {
            User user = findUserById(id);
            user.setDelete(true);
            user.setDeleteTime(new Date());
            users.add(user);
        }

        userMongodbRepository.save(users);
    }
}