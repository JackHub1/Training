package com.augmentu.mediacloud.training.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.augmentu.mediacloud.training.bean.User;
import com.augmentu.mediacloud.training.bean.UserPortraitFilterCondition;
import com.augmentu.mediacloud.training.constants.UserConstants;
import com.augmentu.mediacloud.training.dao.postgresqlDao.UserPostgresqlRepository;
import com.augmentu.mediacloud.training.service.UserService;

@Service
@Profile("postgresql")
public class UserPostgresqlServiceImpl implements UserService {
    @Autowired
    private UserPostgresqlRepository userPostgresqlRepository;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public User saveUser(User user) {
        user.setCreateTime(new Date());
        return userPostgresqlRepository.save(user);
    }

    /**
     * Set filter condition
     * 
     * @param predicates
     * @param root
     * @param builder
     * @param user
     */
    private void setFilterCondition(List<Predicate> predicates, Root<User> root, CriteriaBuilder builder,
            UserPortraitFilterCondition user) {
        Path<String> pathName = null, pathSex = null, pathBeginBirthday = null, pathLastday = null, pathPost = null,
                pathDelete = null;

        if (user.getName() != null && !user.getName().equals("")) {
            pathName = root.get(UserConstants.NAME);
            predicates.add(builder.like(pathName.as(String.class), "%" + user.getName() + "%"));
        }

        if (user.getSex() != null && !user.getSex().equals("")) {
            pathSex = root.get(UserConstants.SEX);
            String gender = null;
            gender = user.getSex().equals(UserConstants.MALE) ? UserConstants.MALE : UserConstants.FAMALE;
            predicates.add(builder.equal(pathSex.as(String.class),gender));
        }

        if (user.getStartDay() != null && !user.getStartDay().equals("")) {
            pathBeginBirthday = root.get(UserConstants.BIRTHDAY);
            predicates.add(builder.greaterThanOrEqualTo(pathBeginBirthday.as(Date.class), user.getStartDay()));
        }

        if (user.getEndDay() != null && !user.getEndDay().equals("")) {
            pathLastday = root.get(UserConstants.BIRTHDAY);
            predicates.add(builder.lessThanOrEqualTo(pathLastday.as(Date.class), user.getEndDay()));
        }

        if (user.getPost() != null && !user.getPost().equals("")) {
            pathPost = root.get(UserConstants.POST);
            predicates.add(builder.like(pathPost.as(String.class), "%" + user.getPost() + "%"));
        }

        pathDelete = root.get(UserConstants.IS_DELETE);
        predicates.add(builder.isFalse(pathDelete.as(Boolean.class)));
    }

    @Override
    public Page<User> findUser(UserPortraitFilterCondition userSearch, Integer pageNum) {

        if(pageNum <= 0) {
            pageNum =1;
        }

        Page<User> users = userPostgresqlRepository.findAll(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<>();
                setFilterCondition(predicates, root, builder, userSearch);
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            }

        }, new PageRequest(pageNum - 1, 10, new Sort(Direction.DESC, UserConstants.CREATE_TIME)));

        return users;
    }

    @Override
    public User findUserById(String id) {
        Integer parseId = null;

        try {
            parseId = Integer.parseInt(id);
        } catch (Exception e) {
            logger.warn("参数转换失败" + e.getMessage());
        }

        User resultUser = userPostgresqlRepository.findOne(parseId);
        return resultUser;
    }

    @Override
    public void deleteMultipleUser(String[] ids) {
        List<Integer> parseIds = new ArrayList<>();

        for (String id : ids) {
            Integer parseId = null;

            try {
                parseId = Integer.parseInt(id);
            } catch (Exception e) {
                logger.warn("参数转换失败" + e.getMessage());
            }

            parseIds.add(parseId);
        }

        userPostgresqlRepository.multipleDeleteUser(parseIds, new Date());
    }

}