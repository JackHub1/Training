package com.augmentu.mediacloud.training.dao.mongodbDao;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.augmentu.mediacloud.training.bean.User;

public interface UserMongodbRepository extends MongoRepository<User, ObjectId> {

    /**
     * Find users and paging the user
     * 
     * @param name
     * @param sex
     * @param firthday
     * @param lastday
     * @param post
     * @param page
     * @return page search User
     */
    @Query("{name: {$regex: ?0}, sex: ?1, birthday: {$gte: ?2, $lte: ?3}, post: {$regex: ?4}, isDelete: false}")
    Page<User> findUser(String name, String sex, Date firstday, Date lastday, String post, Pageable page);

    /**
     * Find the paging user
     */
    @Query("{ isDelete: false}")
    Page<User> findAll(Pageable page);

}
