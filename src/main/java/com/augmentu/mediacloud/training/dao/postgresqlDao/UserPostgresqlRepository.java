package com.augmentu.mediacloud.training.dao.postgresqlDao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.augmentu.mediacloud.training.bean.User;

public interface UserPostgresqlRepository extends CrudRepository<User, Integer>, JpaSpecificationExecutor<User> {

    /**
     * Find user and paging the user
     * 
     * @param page
     * @return paging the user
     */
    Page<User> findAll(Pageable page);

    /**
     *Multiple delete user
     * 
     * @param ids
     * @param deleteTime
     */
    @Modifying
    @Transactional
    @Query("UPDATE User u SET u.isDelete = true, u.deleteTime = ?2 WHERE u.id in ?1")
    void multipleDeleteUser(List<Integer> ids, Date deleteTime);
}