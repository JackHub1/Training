package com.augmentu.mediacloud.training.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.alibaba.fastjson.JSONObject;
import com.augmentu.mediacloud.training.bean.User;
import com.augmentu.mediacloud.training.bean.UserPortraitFilterCondition;
import com.augmentu.mediacloud.training.constants.UserConstants;
import com.augmentu.mediacloud.training.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestController {
    private final static Random random = new Random();

    @MockBean
    UserService userService;
    private MockMvc mockMvc;
    @Autowired
    private UserController userController;

    private User getNewUser() {
        String name = "Test" + random.nextInt(10000);
        User newUser = new User(name, random.nextBoolean() ? UserConstants.MALE : UserConstants.FAMALE, new Date(),
                random.nextBoolean() ? "云南" : "上海", random.nextBoolean() ? "汉族" : "苗族",
                random.nextBoolean() ? "经理" : "职员", new Date(), new Date(), random.nextBoolean() ? true : false);
        return newUser;
    }
    
    /**
     * Get a userGroup
     * @return
     */
    private List<User> getNewUserList() {
        List<User> userGroup = new ArrayList<User>();
        userGroup.add(getNewUser());
        return userGroup;
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    /**
     * 
     * Test findById() in {@link PersonController}.
     * 
     * @throws Exception
     **/
    @Test
    public void testFindById() throws Exception {
        User resultUser = getNewUser();
        BDDMockito.given(userService.findUserById(Mockito.anyString())).willReturn(resultUser);
        mockMvc.perform(get("/users/{id}", 1)).andDo(print()).andExpect(jsonPath("name").value(resultUser.getName()))
                .andExpect(status().isOk());
    }

    /**
     * Test Save user
     * 
     * @throws Exception
     */
    @Test
    public void testSaveUser() throws Exception {
        User user = getNewUser();
        BDDMockito.given(this.userService.saveUser((User) Mockito.anyObject())).willReturn(user);
        String requestJson = JSONObject.toJSONString(user);
        mockMvc.perform(post("/users/").contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk());
    }

    /**
     * Test Delete user 
     * 
     * @throws Exception
     */
    @Test
    public void testDeleteUser() throws Exception {
        mockMvc.perform(delete("/users/delete").param("deleteIdGroup", "1,2,3")).andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * Test update user
     * 
     * @throws Exception
     */
    @Test
    public void testUpdateUser() throws Exception {
        User updatedUser = getNewUser();
        BDDMockito.given(userService.saveUser((User) Mockito.anyObject())).willReturn(updatedUser);
        String resultJson = JSONObject.toJSONString(updatedUser);
        mockMvc.perform(put("/users/").contentType(MediaType.APPLICATION_JSON).content(resultJson)).andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * Test find userGroup
     * 
     * @throws Exception
     */
    @Test
    public void testFindUser() throws Exception {
        Page<User> pageUser = new PageImpl<User>(getNewUserList());
        BDDMockito.given(userService.findUser((UserPortraitFilterCondition) Mockito.anyObject(), Mockito.anyInt())).willReturn(pageUser);
        mockMvc.perform(get("/users/find").param("user", "user").param("pageNum", "1")).andDo(print()).andExpect(status().isOk());
    }
}
