package com.augmentu.mediacloud.training.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.augmentu.mediacloud.training.bean.User;
import com.augmentu.mediacloud.training.constants.UserConstants;
import com.augmentu.mediacloud.training.dao.mongodbDao.UserMongodbRepository;

@Profile("mongodb")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestMongodbRepository {
    private final static Random random = new Random();

    @Autowired
    private UserMongodbRepository repository;
    private User user;

    private User getNewUser() {
        String name = "Test" + random.nextInt(10000);
        User newUser = new User(name, random.nextBoolean() ? UserConstants.MALE : UserConstants.FAMALE, new Date(),
                random.nextBoolean() ? "云南" : "上海", random.nextBoolean() ? "汉族" : "苗族",
                random.nextBoolean() ? "经理" : "职员", new Date(), new Date(), random.nextBoolean() ? true : false);
        return newUser;
    }

    @Transactional
    @Before
    public void setUp() throws Exception {
        user = repository.save(getNewUser());
    }

    /**
     * Test save user
     * 
     * @throws Exception
     */
    @Transactional
    @Test
    public void testSave() throws Exception {
        User saveUser = getNewUser();
        User resultUser = repository.save(saveUser);
        assertNotNull(resultUser);
        assertTrue("The result not true", resultUser.equals(saveUser));
    }

    /**
     * Test update user
     * 
     * @throws Exception
     */
    @Transactional
    @Test
    public void testUpdate() throws Exception {
        User newUser = user;
        user.setCreateTime(new Date());
        User resultUser = repository.save(newUser);
        assertNotNull(resultUser);
        assertTrue("The result not true", resultUser.equals(newUser));
    }

    /**
     * Test find userGroup
     * 
     * @throws Exception
     */
    @Transactional
    @Test
    public void testFindUserGroup() throws Exception {
        Sort sort = new Sort(Sort.Direction.ASC, UserConstants.CREATE_TIME);
        Pageable pageable = new PageRequest(0, 10, sort);
        Page<User> resultUserGroup = repository.findUser(".", ".", new Date(), new Date(), ".", pageable);
        assertNotNull("The test user is null", resultUserGroup);
    }

}
