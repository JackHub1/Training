package com.augmentu.mediacloud.training.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.augmentu.mediacloud.training.bean.User;
import com.augmentu.mediacloud.training.constants.UserConstants;
import com.augmentu.mediacloud.training.dao.postgresqlDao.UserPostgresqlRepository;

@Profile("postgresql")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestPostgresqlRepository {

    private final static Random random = new Random();

    @Autowired
    private UserPostgresqlRepository repository;
    private User user;

    private User getNewUser() {
        String name = "Test" + random.nextInt(10000);
        User newUser = new User(name, random.nextBoolean() ? UserConstants.MALE : UserConstants.FAMALE, new Date(),
                random.nextBoolean() ? "云南" : "上海", random.nextBoolean() ? "汉族" : "苗族",
                random.nextBoolean() ? "经理" : "职员", new Date(), new Date(), random.nextBoolean() ? true : false);
        return newUser;
    }

    @Transactional
    @Before
    public void setUp() throws Exception {
        user = repository.save(getNewUser());
    }

    /**
     * Test save user
     * 
     * @throws Exception
     */
    @Test
    @Transactional
    public void testSave() throws Exception {
        User saveUser = getNewUser();
        User resultUser = repository.save(saveUser);
        assertNotNull("Test user is null", resultUser);
        assertTrue("Test resultUser isn't the test object", saveUser.equals(resultUser));
    }

    /**
     * Test update user
     * 
     * @throws Exception
     */
    @Test
    @Transactional
    public void testUpdate() throws Exception {
        User newUser = user;
        newUser.setCreateTime(new Date());
        User resultUser = repository.save(newUser);
        assertNotNull("Test user is null", resultUser);
        assertTrue("The resultUser not true", resultUser.equals(newUser));
    }

    /**
     * Test Find userGroup
     * 
     * @throws Exception
     */
    @Test
    public void testFindUser() throws Exception {
        Page<User> resultUser = repository.findAll(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> predicates = new ArrayList<>();
                return builder.and(predicates.toArray(new Predicate[predicates.size()]));
            }

        }, new PageRequest(1, 2, new Sort(Direction.DESC, UserConstants.CREATE_TIME)));
        assertNotNull("Test userCondition is null", resultUser);
    }

    /**
     * Test multiple delete user
     * 
     * @throws Exception
     */
    @Test
    @Transactional
    public void testMultipleDeleteUser() throws Exception {
        List<Integer> parseIds = new ArrayList<>();
        parseIds.add(114);
        parseIds.add(119);
        repository.multipleDeleteUser(parseIds, new Date());
    }
}
